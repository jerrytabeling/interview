#!/bin/python

# Question 5:
# Return the results of all pods that are in a running state
# and the namespace in the following format:
# Pod: ${POD_NAME}, Namespace: ${NAMESPACE}

from kubernetes import client, config
import os

def auth():
  if os.environ.get("KUBERNETES_SERVICE_HOST") is not None:
    config.load_incluster_config()
  else:
    config.load_kube_config()

def print_running_pods():
  v1 = client.CoreV1Api()
  pods = v1.list_pod_for_all_namespaces(watch=False)
  for pod in pods.items:
    if pod.status.phase == "Running":
      print("Pod: %s, Namespace: %s" % (pod.metadata.name, pod.metadata.namespace))

def main():
  auth()
  print_running_pods()

if __name__ == "__main__":
  main()

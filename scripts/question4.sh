#!/bin/bash

# Question 4:
# Run kubectl get pods -A, return the results of pods that are running and the namespace
# in the following format:
# Pod: ${POD_NAME}, Namespace: ${NAMESPACE}

kubectl get pods -A | grep Running | awk '{print "Pod: " $2", Namespace: "$1}'
#https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role

resource "aws_iam_role" "prod_iam_role" {
  name               = "prod_iam_role"
  assume_role_policy = data.aws_iam_policy_document.prod_iam_role_assume_role_policy.json
}

data "aws_iam_policy_document" "prod_iam_role_assume_role_policy" {
  statement {
    actions = ["sts:AssumeRole"]
    principals {
        type        = "AWS"
        identifiers = ["arn:aws:iam::${var.account_id}:root"]
    }
  }
}

#https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_group
resource "aws_iam_group" "prod_iam_group" {
  name = "prod_iam_group"
}

#https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_group_policy
resource "aws_iam_group_policy" "prod_iam_group_policy" {
  name  = "prod_iam_group_policy"
  group = aws_iam_group.prod_iam_group.name
  policy = data.aws_iam_policy_document.prod_iam_group_policy.json
}

data "aws_iam_policy_document" "prod_iam_group_policy" {
  statement {
    actions = ["sts:AssumeRole"]
    resources = ["arn:aws:iam::${var.account_id}:role/prod_iam_role"]
  }
}


#https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_user
resource "aws_iam_user" "prod_iam_user" {
  name = "prod_iam_user"
}

#https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_user_group_membership
resource "aws_iam_user_group_membership" "prod_iam_group_membership" {
  user = aws_iam_user.prod_iam_user.name
  groups = [
    aws_iam_group.prod_iam_group.name
  ]
}

# interview

## Questions 1, 2, 3

Related MRs:
- https://gitlab.com/jerrytabeling/interview/-/merge_requests/1
- https://gitlab.com/jerrytabeling/interview/-/merge_requests/2
- https://gitlab.com/jerrytabeling/interview/-/merge_requests/3

Example pipelines:
- running build and deploy stages at the same time: https://gitlab.com/jerrytabeling/interview/-/pipelines/385796645
- running build stages only: https://gitlab.com/jerrytabeling/interview/-/pipelines/385797322
- running deploy stage only: https://gitlab.com/jerrytabeling/interview/-/pipelines/385798134

Litecoin pod log output:

```
➜  interview git:(feature/interview) kubectl -n gitlab-runner logs -f litecoin-0
2021-10-10T21:33:52Z Litecoin Core version v0.18.1 (release build)
2021-10-10T21:33:52Z Assuming ancestors of block b34a457c601ef8ce3294116e3296078797be7ded1b0d12515395db9ab5e93ab8 have valid signatures.
2021-10-10T21:33:52Z Setting nMinimumChainWork=0000000000000000000000000000000000000000000002ee655bf00bf13b4cca
2021-10-10T21:33:52Z Using the 'sse4(1way),sse41(4way),avx2(8way)' SHA256 implementation
2021-10-10T21:33:52Z Using RdRand as an additional entropy source
2021-10-10T21:33:52Z Default data directory /home/litecoin/.litecoin
2021-10-10T21:33:52Z Using data directory /home/litecoin/.litecoin
2021-10-10T21:33:52Z Config file: /home/litecoin/.litecoin/litecoin.conf (not found, skipping)
2021-10-10T21:33:52Z Using at most 125 automatic connections (1048576 file descriptors available)
2021-10-10T21:33:52Z Using 16 MiB out of 32/2 requested for signature cache, able to store 524288 elements
2021-10-10T21:33:52Z Using 16 MiB out of 32/2 requested for script execution cache, able to store 524288 elements
2021-10-10T21:33:52Z Using 8 threads for script verification
2021-10-10T21:33:52Z scheduler thread start
2021-10-10T21:33:52Z libevent: getaddrinfo: address family for nodename not supported
2021-10-10T21:33:52Z Binding RPC on address ::1 port 9332 failed.
2021-10-10T21:33:52Z HTTP: creating work queue of depth 16
2021-10-10T21:33:52Z No rpcpassword set - using random cookie authentication.
2021-10-10T21:33:52Z Generated RPC authentication cookie /home/litecoin/.litecoin/.cookie
2021-10-10T21:33:52Z HTTP: starting 4 worker threads
2021-10-10T21:33:52Z Using wallet directory /home/litecoin/.litecoin/wallets
2021-10-10T21:33:52Z init message: Verifying wallet(s)...
2021-10-10T21:33:52Z Using BerkeleyDB version Berkeley DB 4.8.30: (April  9, 2010)
2021-10-10T21:33:52Z Using wallet /home/litecoin/.litecoin/wallets
2021-10-10T21:33:52Z BerkeleyEnvironment::Open: LogDir=/home/litecoin/.litecoin/wallets/database ErrorFile=/home/litecoin/.litecoin/wallets/db.log
2021-10-10T21:33:52Z init message: Loading banlist...
2021-10-10T21:33:52Z Cache configuration:
2021-10-10T21:33:52Z * Using 2.0 MiB for block index database
2021-10-10T21:33:52Z * Using 8.0 MiB for chain state database
2021-10-10T21:33:52Z * Using 440.0 MiB for in-memory UTXO set (plus up to 286.1 MiB of unused mempool space)
2021-10-10T21:33:52Z init message: Loading block index...
2021-10-10T21:33:52Z Opening LevelDB in /home/litecoin/.litecoin/blocks/index
2021-10-10T21:33:52Z Opened LevelDB successfully
2021-10-10T21:33:52Z Using obfuscation key for /home/litecoin/.litecoin/blocks/index: 0000000000000000
2021-10-10T21:33:52Z LoadBlockIndexDB: last block file = 0
2021-10-10T21:33:52Z LoadBlockIndexDB: last block file info: CBlockFileInfo(blocks=0, size=0, heights=0...0, time=1970-01-01...1970-01-01)
2021-10-10T21:33:52Z Checking all blk files are present...
2021-10-10T21:33:52Z Initializing databases...
2021-10-10T21:33:52Z Pre-allocating up to position 0x1000000 in blk00000.dat
2021-10-10T21:33:52Z Opening LevelDB in /home/litecoin/.litecoin/chainstate
2021-10-10T21:33:52Z Opened LevelDB successfully
2021-10-10T21:33:52Z Using obfuscation key for /home/litecoin/.litecoin/chainstate: 77f583a6dadf5f2b
2021-10-10T21:33:52Z init message: Rewinding blocks...
2021-10-10T21:33:52Z  block index               5ms
2021-10-10T21:33:52Z init message: Loading wallet...
2021-10-10T21:33:52Z BerkeleyEnvironment::Open: LogDir=/home/litecoin/.litecoin/wallets/database ErrorFile=/home/litecoin/.litecoin/wallets/db.log
2021-10-10T21:33:52Z [default wallet] nFileVersion = 180100
2021-10-10T21:33:52Z [default wallet] Keys: 2001 plaintext, 0 encrypted, 2001 w/ metadata, 2001 total. Unknown wallet records: 0
2021-10-10T21:33:52Z [default wallet] Wallet completed loading in              35ms
2021-10-10T21:33:52Z [default wallet] setKeyPool.size() = 2000
2021-10-10T21:33:52Z [default wallet] mapWallet.size() = 0
2021-10-10T21:33:52Z [default wallet] mapAddressBook.size() = 0
2021-10-10T21:33:52Z UpdateTip: new best=12a765e31ffd4059bada1e25190f6e98c99d9714d334efa41a195a7e7e04bfe2 height=0 version=0x00000001 log2_work=20.000022 tx=1 date='2011-10-07T07:31:05Z' progress=0.000000 cache=0.0MiB(0txo)
2021-10-10T21:33:52Z Failed to open mempool file from disk. Continuing anyway.
2021-10-10T21:33:52Z mapBlockIndex.size() = 1
2021-10-10T21:33:52Z nBestHeight = 0
2021-10-10T21:33:52Z torcontrol thread start
2021-10-10T21:33:52Z Bound to [::]:9333
2021-10-10T21:33:52Z Bound to 0.0.0.0:9333
2021-10-10T21:33:52Z init message: Loading P2P addresses...
2021-10-10T21:33:52Z Loaded 0 addresses from peers.dat  0ms
2021-10-10T21:33:52Z init message: Starting network threads...
2021-10-10T21:33:52Z net thread start
2021-10-10T21:33:52Z dnsseed thread start
2021-10-10T21:33:52Z Loading addresses from DNS seeds (could take a while)
2021-10-10T21:33:52Z addcon thread start
2021-10-10T21:33:52Z opencon thread start
2021-10-10T21:33:52Z init message: Done loading
2021-10-10T21:33:52Z msghand thread start
2021-10-10T21:33:57Z 71 addresses found from DNS seeds
2021-10-10T21:33:57Z dnsseed thread exit
2021-10-10T21:33:57Z New outbound peer connected: version: 70015, blocks=2137804, peer=0
2021-10-10T21:34:03Z New outbound peer connected: version: 70015, blocks=2137804, peer=1
2021-10-10T21:34:04Z New outbound peer connected: version: 70015, blocks=2137804, peer=2
2021-10-10T21:34:06Z New outbound peer connected: version: 70015, blocks=2137804, peer=3
2021-10-10T21:34:09Z New outbound peer connected: version: 70015, blocks=2137804, peer=4
2021-10-10T21:34:10Z New outbound peer connected: version: 70015, blocks=2137804, peer=5
2021-10-10T21:34:11Z New outbound peer connected: version: 70015, blocks=2137804, peer=6
2021-10-10T21:34:12Z New outbound peer connected: version: 70015, blocks=2137804, peer=7
```

## Questions 4,5

Related MRs:
- https://gitlab.com/jerrytabeling/interview/-/merge_requests/4
- https://gitlab.com/jerrytabeling/interview/-/merge_requests/5

Bash and Python Script Outputs:
```
➜  scripts git:(feature/interview) ✗ bash question4.sh
Pod: gitlab-runner-gitlab-runner-5f449fffff-phs6l, Namespace: gitlab-runner
Pod: litecoin-0, Namespace: gitlab-runner
Pod: coredns-74ff55c5b-q8vt9, Namespace: kube-system
Pod: etcd-minikube, Namespace: kube-system
Pod: kube-apiserver-minikube, Namespace: kube-system
Pod: kube-controller-manager-minikube, Namespace: kube-system
Pod: kube-proxy-xjkgq, Namespace: kube-system
Pod: kube-scheduler-minikube, Namespace: kube-system
Pod: storage-provisioner, Namespace: kube-system

➜  scripts git:(feature/interview) ✗ python3 question5.py
Pod: gitlab-runner-gitlab-runner-5f449fffff-phs6l, Namespace: gitlab-runner
Pod: litecoin-0, Namespace: gitlab-runner
Pod: coredns-74ff55c5b-q8vt9, Namespace: kube-system
Pod: etcd-minikube, Namespace: kube-system
Pod: kube-apiserver-minikube, Namespace: kube-system
Pod: kube-controller-manager-minikube, Namespace: kube-system
Pod: kube-proxy-xjkgq, Namespace: kube-system
Pod: kube-scheduler-minikube, Namespace: kube-system
Pod: storage-provisioner, Namespace: kube-system
```

## Question 6

- https://gitlab.com/jerrytabeling/interview/-/merge_requests/6
- https://gitlab.com/jerrytabeling/interview/-/merge_requests/8

Terraform init, plan, apply output:
```
➜  terraform git:(feature/interview) ✗ terraform init
Initializing modules...
- question6 in module

Initializing the backend...

Initializing provider plugins...
- Finding latest version of hashicorp/aws...
- Installing hashicorp/aws v3.62.0...
- Installed hashicorp/aws v3.62.0 (signed by HashiCorp)

Terraform has created a lock file .terraform.lock.hcl to record the provider
selections it made above. Include this file in your version control repository
so that Terraform can guarantee to make the same selections by default when
you run "terraform init" in the future.

Terraform has been successfully initialized!

You may now begin working with Terraform. Try running "terraform plan" to see
any changes that are required for your infrastructure. All Terraform commands
should now work.

If you ever set or change modules or backend configuration for Terraform,
rerun this command to reinitialize your working directory. If you forget, other
commands will detect it and remind you to do so if necessary.
➜  terraform git:(feature/interview) ✗ terraform plan

Terraform used the selected providers to generate the following execution plan. Resource actions are indicated with the following symbols:
  + create

Terraform will perform the following actions:

  # module.question6.aws_iam_group.prod_iam_group will be created
  + resource "aws_iam_group" "prod_iam_group" {
      + arn       = (known after apply)
      + id        = (known after apply)
      + name      = "prod_iam_group"
      + path      = "/"
      + unique_id = (known after apply)
    }

  # module.question6.aws_iam_group_policy.prod_iam_group_policy will be created
  + resource "aws_iam_group_policy" "prod_iam_group_policy" {
      + group  = "prod_iam_group"
      + id     = (known after apply)
      + name   = "prod_iam_group_policy"
      + policy = jsonencode(
            {
              + Statement = [
                  + {
                      + Action   = "sts:AssumeRole"
                      + Effect   = "Allow"
                      + Resource = "arn:aws:iam::377763753435:role/prod_iam_role"
                      + Sid      = ""
                    },
                ]
              + Version   = "2012-10-17"
            }
        )
    }

  # module.question6.aws_iam_role.prod_iam_role will be created
  + resource "aws_iam_role" "prod_iam_role" {
      + arn                   = (known after apply)
      + assume_role_policy    = jsonencode(
            {
              + Statement = [
                  + {
                      + Action    = "sts:AssumeRole"
                      + Effect    = "Allow"
                      + Principal = {
                          + AWS = "arn:aws:iam::377763753435:root"
                        }
                      + Sid       = ""
                    },
                ]
              + Version   = "2012-10-17"
            }
        )
      + create_date           = (known after apply)
      + force_detach_policies = false
      + id                    = (known after apply)
      + managed_policy_arns   = (known after apply)
      + max_session_duration  = 3600
      + name                  = "prod_iam_role"
      + name_prefix           = (known after apply)
      + path                  = "/"
      + tags_all              = (known after apply)
      + unique_id             = (known after apply)

      + inline_policy {
          + name   = (known after apply)
          + policy = (known after apply)
        }
    }

  # module.question6.aws_iam_user.prod_iam_user will be created
  + resource "aws_iam_user" "prod_iam_user" {
      + arn           = (known after apply)
      + force_destroy = false
      + id            = (known after apply)
      + name          = "prod_iam_user"
      + path          = "/"
      + tags_all      = (known after apply)
      + unique_id     = (known after apply)
    }

  # module.question6.aws_iam_user_group_membership.prod_iam_group_membership will be created
  + resource "aws_iam_user_group_membership" "prod_iam_group_membership" {
      + groups = [
          + "prod_iam_group",
        ]
      + id     = (known after apply)
      + user   = "prod_iam_user"
    }

Plan: 5 to add, 0 to change, 0 to destroy.

─────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────

Note: You didn't use the -out option to save this plan, so Terraform can't guarantee to take exactly these actions if you run "terraform apply" now.
➜  terraform git:(feature/interview) ✗ terraform apply

Terraform used the selected providers to generate the following execution plan. Resource actions are indicated with the following symbols:
  + create

Terraform will perform the following actions:

  # module.question6.aws_iam_group.prod_iam_group will be created
  + resource "aws_iam_group" "prod_iam_group" {
      + arn       = (known after apply)
      + id        = (known after apply)
      + name      = "prod_iam_group"
      + path      = "/"
      + unique_id = (known after apply)
    }

  # module.question6.aws_iam_group_policy.prod_iam_group_policy will be created
  + resource "aws_iam_group_policy" "prod_iam_group_policy" {
      + group  = "prod_iam_group"
      + id     = (known after apply)
      + name   = "prod_iam_group_policy"
      + policy = jsonencode(
            {
              + Statement = [
                  + {
                      + Action   = "sts:AssumeRole"
                      + Effect   = "Allow"
                      + Resource = "arn:aws:iam::377763753435:role/prod_iam_role"
                      + Sid      = ""
                    },
                ]
              + Version   = "2012-10-17"
            }
        )
    }

  # module.question6.aws_iam_role.prod_iam_role will be created
  + resource "aws_iam_role" "prod_iam_role" {
      + arn                   = (known after apply)
      + assume_role_policy    = jsonencode(
            {
              + Statement = [
                  + {
                      + Action    = "sts:AssumeRole"
                      + Effect    = "Allow"
                      + Principal = {
                          + AWS = "arn:aws:iam::377763753435:root"
                        }
                      + Sid       = ""
                    },
                ]
              + Version   = "2012-10-17"
            }
        )
      + create_date           = (known after apply)
      + force_detach_policies = false
      + id                    = (known after apply)
      + managed_policy_arns   = (known after apply)
      + max_session_duration  = 3600
      + name                  = "prod_iam_role"
      + name_prefix           = (known after apply)
      + path                  = "/"
      + tags_all              = (known after apply)
      + unique_id             = (known after apply)

      + inline_policy {
          + name   = (known after apply)
          + policy = (known after apply)
        }
    }

  # module.question6.aws_iam_user.prod_iam_user will be created
  + resource "aws_iam_user" "prod_iam_user" {
      + arn           = (known after apply)
      + force_destroy = false
      + id            = (known after apply)
      + name          = "prod_iam_user"
      + path          = "/"
      + tags_all      = (known after apply)
      + unique_id     = (known after apply)
    }

  # module.question6.aws_iam_user_group_membership.prod_iam_group_membership will be created
  + resource "aws_iam_user_group_membership" "prod_iam_group_membership" {
      + groups = [
          + "prod_iam_group",
        ]
      + id     = (known after apply)
      + user   = "prod_iam_user"
    }

Plan: 5 to add, 0 to change, 0 to destroy.

Do you want to perform these actions?
  Terraform will perform the actions described above.
  Only 'yes' will be accepted to approve.

  Enter a value: yes

module.question6.aws_iam_group.prod_iam_group: Creating...
module.question6.aws_iam_user.prod_iam_user: Creating...
module.question6.aws_iam_role.prod_iam_role: Creating...
module.question6.aws_iam_user.prod_iam_user: Creation complete after 0s [id=prod_iam_user]
module.question6.aws_iam_group.prod_iam_group: Creation complete after 0s [id=prod_iam_group]
module.question6.aws_iam_group_policy.prod_iam_group_policy: Creating...
module.question6.aws_iam_user_group_membership.prod_iam_group_membership: Creating...
module.question6.aws_iam_group_policy.prod_iam_group_policy: Creation complete after 1s [id=prod_iam_group:prod_iam_group_policy]
module.question6.aws_iam_role.prod_iam_role: Creation complete after 1s [id=prod_iam_role]
module.question6.aws_iam_user_group_membership.prod_iam_group_membership: Creation complete after 1s [id=terraform-20211010235852569000000001]
```

sts assume role via aws cli output:
```

aws sts get-caller-identity


{
    "Account": "XXXXX",
    "UserId": "XXXXX",
    "Arn": "arn:aws:iam::XXXXX:user/prod_iam_user"
}

aws sts assume-role --role-arn "arn:aws:iam::XXXXX:role/prod_iam_role" --role-session-name AWSCLI-Session


{
    "AssumedRoleUser": {
        "AssumedRoleId": "XXXXX:AWSCLI-Session",
        "Arn": "arn:aws:sts::XXXXX:assumed-role/prod_iam_role/AWSCLI-Session"
    },
    "Credentials": {
        "SecretAccessKey": "XXXXX",
        "SessionToken": "XXXXX",
        "Expiration": "2021-10-11T02:03:36Z",
        "AccessKeyId": "XXXXX"
    }
}
```
